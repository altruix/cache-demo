public interface ITimeProvider {
    long getCurrentTime();
}
