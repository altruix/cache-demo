import java.util.Date;

public class TimeProvider implements ITimeProvider {
    @Override
    public long getCurrentTime() {
        return new Date().getTime();
    }
}
