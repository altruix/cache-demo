public interface ICache<K> {
    /**
     * This method is called whenever object with key K has been changed. Therefore,
     * if it was cached, it must be removed from the cache.
     */
    void objectChanged(K aKey) throws InterruptedException;
}
