import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Cache<T,K> implements ICache, IDataSource<T,K> {
    /**
     * Data source, from which we read the data. Every read from that data source is expensive.
     */
    private final IDataSource<T,K> slowDataSource;

    /**
     * The actual cache. This map constantly changes, so we need to make it thread safe.
     */
    private final Map<K,T> dataItemsByKeys = new HashMap<K, T>();

    /**
     * Access times.
     */

    private final Map<Long,K> accessTimesByItemIds = new HashMap<Long, K>();

    /**
     * Size of the cache.
     */
    private final int size;

    /**
     * This flag indicates, whether the object is perfoming changes on shared mutable state
     * (member variables dataItemsByKeys and accessTimesByItemIds).
     */
    private boolean busy = false;

    /**
     * I put the code for getting current time into a separate class in order to make testing
     * easier.
     */
    private ITimeProvider timeProvider;

    public Cache(final IDataSource<T, K> aSlowDataSource, final int aSize)
    {
        this(aSlowDataSource, new TimeProvider(), aSize);
    }

    public Cache(final IDataSource<T, K> aSlowDataSource, final ITimeProvider aTimeProvider,
                 final int aSize)
    {
        slowDataSource = aSlowDataSource;
        timeProvider = aTimeProvider;
        size = aSize;
    }

    @Override
    public T readData(final K aKey) throws InterruptedException {
        if (aKey == null)
        {
            return null;
        }

        T result = null;

        synchronized (this)
        {
            waitUntilIdle();

            startCriticalSection();

            final long now = timeProvider.getCurrentTime();

            /**
             * Update access times
             */
            accessTimesByItemIds.put(now, aKey);

            if (dataItemsByKeys.containsKey(aKey))
            {
                /**
                 * The object is already present in the cache, so we just return it.
                 */
                result = dataItemsByKeys.get(aKey);
            }
            else
            {
                /**
                 * The object is not present in the cache. We need to read it from the slow data
                 * source and update the cache.
                 */
                final T readObject = slowDataSource.readData(aKey);

                if (readObject != null)
                {
                    /**
                     * We've read the object from the slow data source. Now we need to put it into
                     * the cache.
                     */
                    if (dataItemsByKeys.size() < size)
                    {
                        /**
                         * There is enough place in the cache.
                         */
                        dataItemsByKeys.put(aKey, readObject);
                    }
                    else
                    {
                        /**
                         * We need to remove the least recently used object in the cache.
                         */
                        /**
                         * First step - find out the ID of the object,
                         * which was accessed least recently.
                         */
                        final long[] accessTimes = new long[accessTimesByItemIds.keySet().size()];

                        int i=0;
                        for (final Long curTime : accessTimesByItemIds.keySet())
                        {
                            accessTimes[i] = curTime;
                            i++;
                        }

                        /**
                         * Sort the accessTimes array in ascending order. This means that the least
                         * recent access time will be the first element of the sorted array.
                         */
                        Arrays.sort(accessTimes);

                        long leastRecentAccessTime = accessTimes[0];

                        /**
                         * Now we find the key of the object to remove using accessTimesByItemIds map,
                         * with leastRecentAccessTime as the key.
                         */
                        final K keyOfObjectToRemove = accessTimesByItemIds.get(leastRecentAccessTime);

                        /**
                         * Remove the object from cache
                         */
                        dataItemsByKeys.remove(keyOfObjectToRemove);

                        /**
                         * Remove the object from the accessTimesByItemIds map
                         */
                        accessTimesByItemIds.remove(leastRecentAccessTime);

                        /**
                         * Add new object to the cache
                         */
                        dataItemsByKeys.put(aKey, readObject);
                    }
                }

                result = readObject;
            }

            endCriticalSection();
        }

        return result;
    }

    @Override
    public void objectChanged(final Object aKey) throws InterruptedException {
        synchronized (this)
        {
            waitUntilIdle();
            startCriticalSection();
            dataItemsByKeys.remove(aKey);
            endCriticalSection();
        }
    }

    private void endCriticalSection() {
        busy = false;
        notifyAll();
    }

    private void startCriticalSection() {
        busy = true;
    }

    private void waitUntilIdle() throws InterruptedException
    {
        while (busy)
        {
            wait();
        }
    }
}
