/**
 * Interface data source represents an object, from which data are read into cache. We assume
 * that the read operation of classes implementing this interface is expensive and therefore we
 * use the cache.
 */
public interface IDataSource<T,K> {
    T readData(final K aKey) throws InterruptedException;
}
