import org.junit.Test;
import org.powermock.reflect.Whitebox;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CacheTest {

    public static final String DATA_OF_PRIMARY_KEY_1001 = "Data of primary key 1001";
    public static final long PRIMARY_KEY_1001 = 1001L;
    public static final String FIRST_VALUE = "First value";
    public static final String SECOND_VALUE = "Second value";
    public static final String VALUE_OF_1002_L = "Value of 1002L";
    public static final String VALUE_OF_1003_L = "Value of 1003L";
    public static final String VALUE_OF_1004_L = "Value of 1004L";
    public static final String VALUE_OF_1005_L = "Value of 1005L";

    /**
     * In this test we verify that Cache actually reads data from the slow data source,
     * if the respective object is not present in the cache.
     */
    @Test
    public void testReadingFromSlowDatabase() throws InterruptedException {
        final IDataSource slowDataSource = mock(IDataSource.class);

        when(slowDataSource.readData(PRIMARY_KEY_1001)).thenReturn(DATA_OF_PRIMARY_KEY_1001);

        /**
         * The cache will hold string data with long primary key. The size of the cache is 10.
         */
        final Cache<String,Long> objectUnderTest = new Cache<String,Long>(slowDataSource, 10);

        final String actualValue = objectUnderTest.readData(PRIMARY_KEY_1001);

        /**
         * Verify that the cache returns correct data.
         */
        assertThat(actualValue).isNotNull();
        assertThat(actualValue).isEqualTo(DATA_OF_PRIMARY_KEY_1001);

        /**
         * Verify that the readData method of the slow data source has been called.
         */
        verify(slowDataSource, times(1)).readData(PRIMARY_KEY_1001);
    }

    /**
     * In this test we verify that Cache returns the cached value, if it is already present in
     * the map.
     */
    @Test
    public void testCaching() throws InterruptedException {
        final IDataSource slowDataSource = mock(IDataSource.class);

        when(slowDataSource.readData(PRIMARY_KEY_1001)).thenReturn(DATA_OF_PRIMARY_KEY_1001);

        /**
         * The cache will hold string data with long primary key. The size of the cache is 10.
         */
        final Cache<String,Long> objectUnderTest = new Cache<String,Long>(slowDataSource, 10);

        /**
         * Read the data first time.
         */
        final String actualValue1 = objectUnderTest.readData(PRIMARY_KEY_1001);

        assertThat(actualValue1).isNotNull();
        assertThat(actualValue1).isEqualTo(DATA_OF_PRIMARY_KEY_1001);

        /**
         * Since it is the first time the record with ID PRIMARY_KEY_1001 is retrieved,
         * we expect Cache to contact slowDataSource once.
         */
        verify(slowDataSource, times(1)).readData(PRIMARY_KEY_1001);

        /**
         * Get the value of PRIMARY_KEY_1001 second time.
         */
        final String actualValue2 = objectUnderTest.readData(PRIMARY_KEY_1001);

        assertThat(actualValue2).isNotNull();
        assertThat(actualValue2).isEqualTo(DATA_OF_PRIMARY_KEY_1001);

        /**
         * This time Cache should have returned data from the map, and the number of interactions
         * with the slow data source should still be equal to 1.
         */
        verify(slowDataSource, times(1)).readData(PRIMARY_KEY_1001);
    }

    /**
     * In the next test we check that objectChanged call removes the value from the map.
     */
    @Test
    public void testObjectChanged() throws InterruptedException {
        final IDataSource slowDataSource = mock(IDataSource.class);

        /**
         * slowDataSource.readData returns "First value" when it is called the first time,
         * and "Second value", when it is called the second time.
         */
        when(slowDataSource.readData(PRIMARY_KEY_1001)).thenReturn(FIRST_VALUE).thenReturn
                (SECOND_VALUE);

        /**
         * Create cache
         */
        final Cache<String,Long> objectUnderTest = new Cache<String,Long>(slowDataSource, 10);

        /**
         * Read the data from slowDataSource (first time)
         */
        assertThat(objectUnderTest.readData(PRIMARY_KEY_1001)).isEqualTo(FIRST_VALUE);

        /**
         * Cache should have contacted the slowDataSource
         */
        verify(slowDataSource, times(1)).readData(PRIMARY_KEY_1001);

        /**
         * Read the data from cache
         */
        assertThat(objectUnderTest.readData(PRIMARY_KEY_1001)).isEqualTo(FIRST_VALUE);

        /**
         * This time, Cache should not have contacted the slow data source,
         * so the number if invocations should remain 1.
         */
        verify(slowDataSource, times(1)).readData(PRIMARY_KEY_1001);

        /**
         * Now let's tell the cache that the value of PRIMARY_KEY_1001 has changed.
         */
        objectUnderTest.objectChanged(PRIMARY_KEY_1001);

        /**
         * Read the data again
         */
        assertThat(objectUnderTest.readData(PRIMARY_KEY_1001)).isEqualTo(SECOND_VALUE);

        /**
         * Verify that Cache contacted the slowDataSource this time (number of invocations should
         * be 1+1 = 2).
         */
        verify(slowDataSource, times(2)).readData(PRIMARY_KEY_1001);
    }

    /**
     * In this test we verify that least recently accessed objects are actually removed from the
     * cache.
     */
    @Test
    public void testRemovalOfObjects() throws InterruptedException {
        final ITimeProvider timeProvider = mock(ITimeProvider.class);
        when(timeProvider.getCurrentTime()).thenReturn(1L).thenReturn(2L).thenReturn(3L).thenReturn
                (4L);

        final IDataSource<String,Long> slowDataSource = mock(IDataSource.class);
        when(slowDataSource.readData(1002L)).thenReturn(VALUE_OF_1002_L);
        when(slowDataSource.readData(1003L)).thenReturn(VALUE_OF_1003_L);
        when(slowDataSource.readData(1004L)).thenReturn(VALUE_OF_1004_L);
        when(slowDataSource.readData(1005L)).thenReturn(VALUE_OF_1005_L);

        final Cache<String,Long> objectUnderTest = new Cache<String,Long>(slowDataSource,
                timeProvider, 2);

        /**
         * First we read data for 1002L and 1003L to fill our cache of 2
         */
        objectUnderTest.readData(1002L);
        objectUnderTest.readData(1003L);

        /**
         * Now let's look what the cache map contains at the moment. We use Whitebox class in
         * order to access the private field via reflection.
         */
        final Map dataItemsByKeys = Whitebox.getInternalState(objectUnderTest, "dataItemsByKeys");

        assertThat(dataItemsByKeys).isNotNull();
        assertThat(dataItemsByKeys.keySet().size()).isEqualTo(2);
        assertThat(dataItemsByKeys.get(1002L)).isEqualTo(VALUE_OF_1002_L);
        assertThat(dataItemsByKeys.get(1003L)).isEqualTo(VALUE_OF_1003_L);

        /**
         * Read another value.
         */
        objectUnderTest.readData(1004L);

        /**
         * Since our cache size is 2, and we read a third object,
         * the least recently used object (the one with key 1002L) should be removed from the
         * cache.
         */
        assertThat(dataItemsByKeys.containsKey(1002L)).isFalse();

        /**
         * Number of objects in the cache should still be equal to 2.
         */
        assertThat(dataItemsByKeys.keySet().size()).isEqualTo(2);

        /**
         * And there should be values of keys 1003L and 1004L
         */
        assertThat(dataItemsByKeys.get(1003L)).isEqualTo(VALUE_OF_1003_L);
        assertThat(dataItemsByKeys.get(1004L)).isEqualTo(VALUE_OF_1004_L);

        /**
         * Read another value
         */
        objectUnderTest.readData(1005L);

        /**
         * Now the cache should contain values for keys 1004L and 1005L
         */
        assertThat(dataItemsByKeys.get(1004L)).isEqualTo(VALUE_OF_1004_L);
        assertThat(dataItemsByKeys.get(1005L)).isEqualTo(VALUE_OF_1005_L);

        /**
         * Value of key 1003L should not be present in the cache.
         */
        assertThat(dataItemsByKeys.containsKey(1003L)).isFalse();
    }
}
