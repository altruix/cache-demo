**GOALS**

1. Create a thread-safe implementation of a LRU cache with size-based eviction. 
2. Provide a short README describing the way it works.
3. Configure project so it can be build by Maven. A simple test demonstrating the functionality should be part of the "verify" phase during the build.

**HOW TO SUBMIT**

Zipped project by email or using github/bitbucket etc.

**CONSTRAINTS**

You can use any collection and/or sychronization mechanism from Core Java. 3rd party libraries are not allowed.

* * *

Description of the implementation

I assume that I have to create a cache with a fixed size of cached elements. When that size is exceeded, the least recently accessed element should be removed from the cache.

In my implementation there is an interface called IDataSource, which represents a data source with expensive access. This may be, for example, a database.

public interface IDataSource<T,K> {
    T readData(final K aKey) throws InterruptedException;
}

When you invoke readData method, an expensive operation (reading a record with primary key aKey from a database) is performed.

The purpose of the cache is to minimize the number of these expensive operations.

The cache itself is located in class Cache.

Its state consists of following members:

1) private final IDataSource<T,K> slowDataSource;

The data source, calls to which should be minimized.

2) private final Map<K,T> dataItemsByKeys = new HashMap<K, T>();

The actual cache. Key is the primary key of the object, T - type of the object.

3)  private final Map<Long,K> accessTimesByItemIds = new HashMap<Long, K>();

Information about when an object with a particular key was accessed the last time.

4) private final int size;

Size of the cache (maximal number of key-value pairs in dataItemsByKeys).

5) private boolean busy = false;

Is used for implementing thread safety. If busy is true, this means that one thread currently accesses the dataItemsByKeys and/or accessTimesByItemIds members, and other threads must wait, until it is finished.

6) private ITimeProvider timeProvider;

This object is used to find out current time (as a long value).  We need this information in order to determine the least recently accessed object in the cache.

The Cache class has two public methods:

1) public T readData(final K aKey) - reads the value of an object, identified by key aKey either from slowDataSource or from the cache.

2) public void objectChanged(final Object aKey) - this method notifies the Cache object that the value of the object identified by key aKey has been changed (e. g. after an UPDATE operation), hence, the value of this object must be removed from the cache.

Both methods modify the state, therefore they must be thread safe.

All thread safe operations are implemented like this:

    waitUntilIdle();
    synchronized (this)
    {
        startCriticalSection();
        // Modify object's state
        endCriticalSection();
    }

waitUntilIdle() waits until busy is equal to false.

	private void waitUntilIdle() throws InterruptedException
    {
        while (busy)
        {
            wait();
        }
    }

startCriticalSection() sets busy to true (thus preventing other thread from accessing the state, while one thread modifies it).

endCriticalSection() sets busy to false and notifies all waiting threds that now one of them can use readData or objectChanged methods of this class.

    private void endCriticalSection() {
        busy = false;
        notifyAll();
    }

How the readData method works, is explained in the comments in the source code.

There are also some unit tests in the CacheTest file.

1) testReadingFromSlowDatabase tests that Cache actually reads data from the slow data source, if no cached data is there.

2) testCaching verifies that Cache returns the cached object, if it is already present in the cache (i. e. doesn't read same object from slow data source twice).

3) testObjectChanged verifies that objectChanged actually removes the changed object from the cache.

4) testRemovalOfObjects verifies that if the cache gets full, least recently used objects are removed from it.